#!/bin/bash
	# Simples MySQL-BKP Script para usar com CRON

	date=$(date +"%Y-%m-%d")
	# Modifique com os seus dados
	db_backup="/home/user/mysql_bkp"
	db_user="root"
	db_pass="*"
	mysql_db="nome da Base"
	mysql_table="nome da Tabela"
	bkp_file="bkp_mysql_$mysql_db-$mysql_table-$date.sql"

	mysqldump -u "$db_user" -p"$db_pass" --routines --extended-insert=FALSE "$mysql_db" "$mysql_table" > "$bkp_file"
	#mysqldump --routines --extended-insert=FALSE --all-databases -u root -p > nomearquivo.sql

exit 0
