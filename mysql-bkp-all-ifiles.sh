#!/bin/sh
# 2014/09/04
# Everlon Passos

BACKUPS=/home/everlon/backups

MYSQL="$(which mysql)"
MYSQLDUMP="$(which mysqldump)"

HOST="localhost"
USER="root"
PASS=""

NOW=$(date +"%Y%m%d")

if [ ! -d $BACKUPS/$NOW/ ]; then
  echo "Creating $BACKUPS/$NOW"
  mkdir -p $BACKUPS/$NOW/
fi

DBS="$($MYSQL -h$HOST -u$USER -p$PASS -Bse 'show databases')"

for db in $DBS; do

     if [ "$db" != "information_schema" ]; then

         time=$(date +"%H-%M-%S")
         dump="$db-$time.sql"

         $MYSQLDUMP -h$HOST -u$USER -p$PASS --routines --extended-insert=FALSE --add-locks $db > $BACKUPS/$NOW/$dump

         echo "$BACKUPS/$NOW/$dump Salvo!"

     fi

done
